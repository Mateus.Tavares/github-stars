## Starting the project

The project is able to be run on development or production mode.

### `Development mode`

To run the app in the development mode you need to run the following commands<br>
````javascript
npm install
npm run postinstall
npm start
````

Development build is used during to development, the page will reload if you any make changes. You will also see any lint errors in the console.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser. 

### `Production mode`

To run the app in the development mode you need to run the following commands.
````javascript
npm install -g serve
serve -s build
````

Production build has better performance and is the version the end user will experience.

Open [http://localhost:5000](http://localhost:5000) to view it in the browser. 

## About the project

Upon first running the project you will have to enter a person token from Github to login. Step by step instructions can be found [here](https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/).

After logging in your personal token will be save for auto-logins in the future, until you sign out.

The project allows you to search for Github users, returning the first user found. Upon finding a user, their information will be displayed along with all their starred repositories. You are able to star repositories to the logged in account as well as unstar repositories.

## Feature Justification

The following are justifications for the project structure and code.

#### `Components`

The creation of multiple components as atoms and molecules allows the project to scale easily. Components are strictly visual, any functional parts of the component should be passed through to the parent page, where the function will be executed. I prefer this approach since it leads to less complicated components that are always changing, making them harder to maintain and scale. This allows us to focus on maintaining a visual library in components that are easily adaptable to new layouts, and new function calls.

#### `Utils`

Since I take away the functional aspect of components I create utils that are reusable blocks of code that execute the functional side of components, while also being useful to use without components. The strength of focusing on the functional aspect to utils is that a component may have different functions while maintaining similar visual appearance, in this case we are able to maintain a single component but 'x' amount of utils to execute certain functions through the component. For large scale projects this makes functional code easier to maintain and easier to locate and fix bugs.

#### `Clients`

The use of clients to concentrate all API calls allows us to easily maintain our API usage, allowing us to easily modify functions as more information is needs as well as allowing us to access all APIs easily from any code.