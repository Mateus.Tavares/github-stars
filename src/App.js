import React, { Component } from 'react';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Login from './containers/Login';
import Search from './containers/Search';

import Loading from '@atoms/Loading';
import { authAutoLogin } from '@actions';


import './style/reset.scss';

class App extends Component {
  componentDidMount = () => {
    this.props.autoLogin();
  }

  render() {
    const { isLoggedIn } = this.props;
    let returnedRoutes = <Loading/>

    if(isLoggedIn != null) {
      if(isLoggedIn) {
        returnedRoutes = (
          <Switch>
            <Route path='/' exact name='Search' component={Search} />
            <Redirect to='/'/>
          </Switch>
        )
      } else {
        returnedRoutes = (
          <Switch>
            <Route path='/' exact name='Login' component={Login} />
            <Redirect to='/'/>
          </Switch>
        )
      }
    }

    return returnedRoutes;
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.auth.isLoggedIn
  };
};

const mapDispatchToProps = dispatch => {
  return {
    autoLogin: () => dispatch(authAutoLogin())
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
