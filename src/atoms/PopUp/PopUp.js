import React from 'react';
import { PropTypes } from 'prop-types';

import Button from '../Button';
import './PopUp.scss'

let closeTimeout;
const PopUp = props => {
  const { buttonText, dismissPopUp, isActive, messageText, popUpKey, onAction } = props;

  if(!isActive) {
    clearTimeout(closeTimeout);
  } else {
    closeTimeout = setTimeout(() => {
      dismissPopUp(popUpKey);
    }, 10000);
  }

  return (
    <div className={`PopUp${isActive ? ' active' : ''}`}>
      <h2 className='PopUp__message content'>{messageText}</h2>
      <Button
        onClick={() => onAction(popUpKey)}
        type='type'
      >
        {buttonText}
      </Button>
    </div>
  );
};

PopUp.propTypes = {
  buttonText: PropTypes.string,
  dismissPopUp: PropTypes.func,
  isActive: PropTypes.bool,
  messageText: PropTypes.string,
  popUpKey: PropTypes.string,
  onAction: PropTypes.func,
};

PopUp.defaultProps = {
  buttonText: '',
  isActive: false,
  messageText: '',
  popUpKey: '',
};

export default PopUp;