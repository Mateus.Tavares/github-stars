import React from 'react';

import './Logo.scss'

const Logo = () => {
  return  (
    <h1 className='logo'>Github<span>Stars</span></h1>
  );
}

export default Logo;