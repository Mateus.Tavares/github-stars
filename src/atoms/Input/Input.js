import React from 'react';
import { PropTypes } from 'prop-types';

import './Input.scss'

const Input = props => {
  const{ customClass, formKey, id, inputConfig, onBlur, onChange, onFocus, submitInput, value } = props;

  return  (
    <input
      {...inputConfig}
      autoComplete='off'
      aria-label={id}
      className={customClass}
      id={id}
      name={id}
      onBlur={() => {
        if(onBlur != null) {
          onBlur()
        }
      }}
      onChange={(e) => onChange(e, id, formKey)}
      onFocus={() => {
        if(onFocus) {
          onFocus()
        }
      }}
      onKeyPress={(e) => {
        if(submitInput != null) {
          if(e.key === 'Enter') submitInput(formKey)
        }
      }}
      value={value}
    />
  );
}

Input.propTypes = {
  customClass: PropTypes.string,
  formKey: PropTypes.string,
  id: PropTypes.string,
  inputConfig: PropTypes.object,
  onChange: PropTypes.func,
  submitInput: PropTypes.func,
  value: PropTypes.string,
};

Input.defaultProps = {
  customClass: '',
  formKey: '',
  id: '',
  inputConfig: {},
  value: '',
};

export default Input;