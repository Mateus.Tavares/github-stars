import React from 'react';
import { PropTypes } from 'prop-types';

import './Button.scss'

const Button = props => {
  const { active, customClass, disabled, onClickParam, onClick } = props;
  return (
    <button 
      className={`btn${active ? ' active' : ''}${customClass !== '' ? ` ${customClass}` : ''}`}
      disabled={disabled}
      type='button'
      role={props.children}
      onClick={() => onClick(onClickParam)}
    >
      {props.children}
    </button>
  );
};

Button.propTypes = {
  active: PropTypes.bool,
  customClass: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  active: false,
  customClass: '',
  disabled: false,
};

export default Button;