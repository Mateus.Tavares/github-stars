import React from 'react';
import { PropTypes } from 'prop-types';

import './Photo.scss'

const Photo = props => {
  const { customClass, imgTitle, imgSize, imgSrc } = props;

  return (
    <img
      alt={imgTitle}
      className={`img ${imgSize}${customClass ? ` ${customClass}` : ''}`}
      src={imgSrc}
    />
  );
};

Photo.propTypes = {
  customClass: PropTypes.string,
  imgTitle: PropTypes.string,
  imgSize: PropTypes.string,
  imgSrc: PropTypes.string
};

Photo.defaultProps = {
  customClass: '',
  imgTitle: '',
  imgSize: 'lg',
  imgSrc: ''
};

export default Photo;
