import React from 'react';
import { PropTypes } from 'prop-types';

import Icon from '@atoms/Icon';
import Button from '@atoms/Button';
import { icons } from '@utils';
import './Card.scss';

const Profile = props => {
  const { handleStarRepository, repository } = props;
  return (
    <div 
      className='card'
    >
      <div className='card__info'>
        <h2 className='title'>
          {repository.name}
        </h2>
        <p className='content'>
          {repository.description}
        </p>
        <div className='card__details'>
          {
            repository.primaryLanguage
            ? (
                <p className='content'>
                  <span 
                    className='card__circle' 
                    style={{ backgroundColor: repository.primaryLanguage.color }}
                  />
                  {repository.primaryLanguage.name}
                </p>
              )
            : null
          }
          <p className='content'>
            <Icon
              icon={icons.star}
              height={16}
              width={16}
              viewHeight={18}
              viewWidth={18}
            />
            {repository.stargazers}
          </p>
        </div>
      </div>
      <Button
        active={repository.viewerHasStarred}
        type='button'
        onClick={handleStarRepository}
        onClickParam={{ id: repository.id, viewerHasStarred: repository.viewerHasStarred }}
      >
        {
          repository.viewerHasStarred
          ? 'unstar'
          : 'star'
        }
      </Button>
    </div>
  );
}

Profile.propTypes = {
  repository: PropTypes.object
};

Profile.defaultProps = {
  repository: {
    description: '',
    id: '',
    name: '',
    language: '',
    stargazers: '',
    viewerHasStarred: ''
  }
};

export default Profile;