import React from 'react';
import { PropTypes } from 'prop-types';

const Icon = props => {
  const { customClass, color, icon, height, onClick, viewHeight, viewWidth, width } = props;
  return (
    <svg
      className={customClass}
      width={`${width}px`}
      height={`${height}px`}
      viewBox={`0 0 ${viewWidth} ${viewHeight}`}
      onClick={onClick != null ? () => onClick() : null}>
      { 
        icon.map((obj, i) => {
          const objType = Object.keys(obj);
          let element;
          switch (objType[0]) {
            case 'path':
              element = <path
                key={i}
                fill={color}
                d={obj.path}/>
              break;
            case 'polygon':
              element = <polygon
                key={i}
                fill={color}
                points={obj.polygon}/>
              break;
            case 'rect':
              element = <rect
                key={i}
                fill={color}
                x={obj.rect.x}
                y={obj.rect.y}
                width={obj.rect.width}
                height={obj.rect.height}/>
              break;
            default:
              element = <path
                key={i}
                fill={color}
                d={obj.path}/>
              break;
          }
          return element;
        })
      }
    </svg>
  );
};

Icon.propTypes = {
  customClass: PropTypes.string,
  color: PropTypes.string,
  icon: PropTypes.array.isRequired,
  height: PropTypes.number,
  onClick: PropTypes.func,
  viewHeight: PropTypes.number,
  viewWidth: PropTypes.number,
  width: PropTypes.number
};

Icon.defaultProps = {
  customClass: '',
  color: '#fff',
  height: 30,
  viewHeight: 30,
  viewWidth: 30,
  width: 30
};

export default Icon;