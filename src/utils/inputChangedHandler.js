import { updateObject } from './';

export const inputChangedHandler = (event, inputId, state) => {
  const inputForm = {...state};
  const currentInput = {...state[inputId]};

  const updatedFormElement = updateObject(currentInput, {
    value: event
  });

  const updatedForm = updateObject(inputForm, {
    [inputId]: updatedFormElement,
  });

  return updatedForm;
}