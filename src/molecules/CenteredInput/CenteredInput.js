import React from 'react';
import { PropTypes } from 'prop-types';

import Logo from '@atoms/Logo';
import InputBox from '@molecules/InputBox';
import './CenteredInput.scss';

const CenteredInput = props => {
  const { inputChangedHandler, inputForm, inputField, submitInputHandler } = props;

  return (
    <div className='CenteredInput'>
      <Logo/>
      <InputBox
        boxType='search'
        formKey={inputForm.key}
        id={inputField}
        inputConfig={inputForm[inputField].inputConfig}
        inputType={inputForm[inputField].inputType}
        onChange={inputChangedHandler}
        onSubmit={submitInputHandler}
        submitInput={submitInputHandler}
        value={inputForm[inputField].value}
      />
    </div>
  );
}

CenteredInput.propTypes = {
  inputChangedHandler: PropTypes.func,
  inputForm: PropTypes.object,
  inputField: PropTypes.string,
  submitInputHandler: PropTypes.func,
};

CenteredInput.defaultProps = {
  inputField: '',
};

export default CenteredInput;
