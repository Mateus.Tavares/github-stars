import React from 'react';
import { PropTypes } from 'prop-types';

import Card from '@atoms/Card';
import Input from '@atoms/Input';
import NoResultsFound from '@molecules/NoResultsFound';
import './CardList.scss';

const Profile = props => {
  const { inputConfig, inputField, inputForm, handleStarRepository, inputChangedHandler, starredRepositories, submitInputHandler, value } = props;

  return (
    <div className='CardList'>
      <Input
        formKey={inputForm.key}
        // inputType={inputField}
        inputConfig={inputConfig}
        id={inputField}
        onChange={(e) => inputChangedHandler(e, inputField, inputForm.key)}
        submitInput={() => { if(submitInputHandler != null) submitInputHandler(inputField) }}
        value={value}
      />
      {
        starredRepositories.length > 0
        ?
          starredRepositories.map(repository => {
            return (
              <Card
                key={repository.id}
                repository={repository}
                handleStarRepository={handleStarRepository}
              />
            )
          })
        : <NoResultsFound
            errorMessage="User doesn't have any starred repositories "
          />
      }
    </div>
  );
}

Profile.propTypes = {
  handleStarRepository: PropTypes.func,
  starredRepositories: PropTypes.array
};

Profile.defaultProps = {
  starredRepositories: []
};

export default Profile;