import React from 'react';
import { PropTypes } from 'prop-types';

import Icon from '@atoms/Icon';
import { icons } from '@utils';
import './NoResultsFound.scss';

const NoResultsFound = props => {
  const { errorMessage } = props;

  return (
    <div className='NoResults'>
      <Icon
        icon={icons.sadFace}
        height={81}
        width={92}
        viewHeight={81}
        viewWidth={92}
      />
      <h2 className='title'>
        {errorMessage}
      </h2>
    </div>
  );
}

NoResultsFound.propTypes = {
  errorMessage: PropTypes.string,
};

NoResultsFound.defaultProps = {
  errorMessage: 'No results found.',
};

export default NoResultsFound;
