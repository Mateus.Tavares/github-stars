import React from 'react';
import { PropTypes } from 'prop-types';

import Button from '@atoms/Button';
import Input from '@atoms/Input';
import Icon from '@atoms/Icon';
import { icons } from '@utils';
import './InputBox.scss';

const InputBox = props => {
  const{ boxType, customClass, formKey, id, inputConfig, inputType, onChange, onSubmit, submitInput, value } = props;
  let boxButtons = null;

  switch (boxType) {
    case 'login':
      boxButtons = (
        <Button 
          type='button'
          onClick={onSubmit}
        >
          Login
        </Button>
      );
      break;
    case 'search':
      boxButtons = (
        <button 
          aria-label='search'
          className='InputBox__search-btn'
          type='submit'
          onClick={onSubmit}
          role='search'
        >
          <Icon 
            icon={icons.search}
            height={18}
            width={20}
            viewHeight={18}
            viewWidth={20}
          />
        </button>
      );
      break;
    default:
      break;
  }

  let inputBoxElement = (
    <div className={`InputBox${customClass ? ` ${customClass}` : ''}`}>
      <label htmlFor={id} className='hide'>{boxType}</label>
      <Input
        formKey={formKey}
        inputType={inputType}
        inputConfig={inputConfig}
        id={id}
        onChange={(e) => onChange(e, id, formKey)}
        submitInput={() => { if(submitInput != null)submitInput(id) }}
        value={value}
      />
        {boxButtons}
    </div>
  );

  return inputBoxElement;
}

InputBox.propTypes = {
  boxType: PropTypes.string,
  customClass: PropTypes.string,
  formKey: PropTypes.string,
  id: PropTypes.string,
  inputConfig: PropTypes.object,
  inputType: PropTypes.string,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  submitInput: PropTypes.func,
  value: PropTypes.string,
};

InputBox.defaultProps = {
  boxType: '',
  customClass: '',
  formKey: '',
  id: '',
  inputConfig: {},
  inputType: 'input',
  value: '',
};

export default InputBox;