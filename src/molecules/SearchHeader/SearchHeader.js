import React from 'react';
import { PropTypes } from 'prop-types';

import Button from '@atoms/Button';
import Logo from '@atoms/Logo';
import Photo from '@atoms/Photo';
import InputBox from '@molecules/InputBox';
import './SearchHeader.scss';

const SearchHeader = props => {
  const { inputChangedHandler, inputForm, inputField, onLogout, submitInputHandler, userName, userPhoto } = props;

  return (
    <div className='SearchHeader'>
      <Logo/>
      <InputBox
        boxType='search'
        formKey={inputForm.key}
        id={inputField}
        inputConfig={inputForm[inputField].inputConfig}
        inputType={inputForm[inputField].inputType}
        onChange={inputChangedHandler}
        onSubmit={submitInputHandler}
        submitInput={submitInputHandler}
        value={inputForm[inputField].value}
      />
      <div className='SearchHeader__nav'>
        <Photo
          imgTitle={userName}
          imgSize='md'
          imgSrc={userPhoto}
        />
        <Button 
          type='button'
          onClick={onLogout}
        >
          Logout
        </Button>
      </div>
    </div>
  );
}

SearchHeader.propTypes = {
  inputChangedHandler: PropTypes.func,
  inputForm: PropTypes.object,
  inputField: PropTypes.string,
  onLogout: PropTypes.func,
  submitInputHandler: PropTypes.func,
  userName: PropTypes.string,
  userPhoto: PropTypes.string
};

SearchHeader.defaultProps = {
  inputField: '',
  userName: '',
  userPhoto: ''
};

export default SearchHeader;