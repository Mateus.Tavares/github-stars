import React from 'react';
import { PropTypes } from 'prop-types';

import Icon from '@atoms/Icon';
import Photo from '@atoms/Photo';
import { icons } from '@utils';
import './Profile.scss';

const Profile = props => {
  const{ userInformation } = props;
  
  const header = (
    <div className='profile__header'>
      <Photo
        imgTitle={userInformation.name}
        imgSize='lg'
        imgSrc={userInformation.photo}
      />
      <h2 className='title'>
        {userInformation.name}
      </h2>
      <h3 className='title light'>
        {userInformation.login}
      </h3>
    </div>
  );

  const about = (
    <div className='profile__about'>
      {
        userInformation.bio
        ? (
            <p className='content bio'>
              {userInformation.bio}
            </p>
          )
        : null
      }
      {
        userInformation.company
        ? (
            <p className='content'>
              <Icon
                icon={icons.users}
                height={15}
                width={17}
                viewHeight={15}
                viewWidth={17}
              />
              {userInformation.company}
            </p>
          )
        : null
      }
      {
        userInformation.location
        ? (
            <p className='content'>
              <Icon
                icon={icons.mapPin}
                height={18}
                width={14}
                viewHeight={18}
                viewWidth={14}
              />
              {userInformation.location}
            </p>
          )
        : null
      }
      {
        userInformation.email
        ? (
            <p className='content'>
              <Icon
                icon={icons.mail}
                height={13}
                width={16}
                viewHeight={13}
                viewWidth={16}
              />
              {userInformation.email}
            </p>
          )
        : null
      }
      {
        userInformation.url
        ? (
            <a href={userInformation.url} className='content'>
              <Icon
                icon={icons.globe}
                height={16}
                width={16}
                viewHeight={16}
                viewWidth={16}
              />
              {userInformation.url}
            </a>
          )
        : null
      }
    </div>
  )

  return (
    <div className='profile'>
      {header}
      {about}
    </div>
  );
}

Profile.propTypes = {
  userInformation: PropTypes.object
};

Profile.defaultProps = {
  userInformation: {
    bio: '',
    company: '',
    email: '',
    id: '',
    location: '',
    login: '',
    name: '',
    photo: '',
    url: '',
    starredRepos: []
  }
};

export default Profile;