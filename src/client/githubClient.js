import axios from 'axios';

const baseURL = 'https://api.github.com/graphql'

const loginToGitHub = async(token) => {
  const header = {
    headers: { Authorization: `bearer ${token}` }
  }
  const query = `{
    viewer { 
      avatarUrl,
      login,
      name
    }
  }`
  const res = await axios.post(baseURL, { query: query }, header);
  return res;
}

const handleStar = async(token, repositoryId, action) => {
  const header = {
    headers: { Authorization: `bearer ${token}` }
  }
  const STAR_QUERY = `
    mutation ($repositoryId: ID!) {
      ${action}(input:{starrableId:$repositoryId}) {
        starrable {
          viewerHasStarred
        }
      }
    }
  `;

  const res = await axios.post(baseURL, { query: STAR_QUERY, variables: { repositoryId }, }, header);
  return res;
}

const searchUsers = async(token, searchTerm) => {
  const header = {
    headers: { Authorization: `bearer ${token}` }
  }
  const query = `{
    search (query: ${searchTerm}, type: USER, first: 1){
      edges {
        node {
          ... on User {
            avatarUrl,
            bio,
            company,
            email,
            id,
            location,
            login,
            name,
            url,
            starredRepositories(first: 100) {
              edges {
                node {
                  description,
                  id,
                  nameWithOwner,
                  viewerHasStarred,
                  languages(first: 30) {
                    edges {
                      node {
                        name
                      }
                    }
                  },
                  primaryLanguage {
                    id
                    name
                    color
                  },
                  stargazers {
                    totalCount
                  }
                }
              }
            }
          }
        }
      }
    }
  }`
  const res = await axios.post(baseURL, { query: query }, header);
  return res;
}

export {
  loginToGitHub,
  handleStar,
  searchUsers
}