import React, { Component } from 'react';
import { connect } from 'react-redux';

import Loading from '@atoms/Loading';
import PopUp from '@atoms/PopUp';
import CardList from '@molecules/CardList';
import CenteredInput from '@molecules/CenteredInput';
import NoResultsFound from '@molecules/NoResultsFound';
import Profile from '@molecules/Profile';
import SearchHeader from '@molecules/SearchHeader';
import { inputChangedHandler, updateObject } from '@utils';
import { githubClient } from '@client';
import { logout } from '@actions';

import './Search.scss';

class Login extends Component {
  state = {
    isLoading: false,
    hasFoundUser: false,
    hasSearched: false,
    popUp: {
      key: 'popUp',
      isActive: false,
      buttonText: 'dismiss',
      messageText: 'Repo starred with success!',
    },
    searchForm: {
      key: 'searchForm',
      searchTerm: {
        id: 'searchTerm',
        inputConfig: {
          name: 'text',
          type: 'text',
          placeholder: 'github username...'
        },
        value: ''
      },
      langFilter: {
        id: 'langFilter',
        inputConfig: {
          name: 'text',
          type: 'text',
          placeholder: 'filter by languages...'
        },
        value: ''
      }
    },
    userInformation: {
      bio: '',
      company: '',
      email: '',
      id: '',
      location: '',
      login: '',
      name: '',
      photo: '',
      url: '',
      starredRepositories: [],
      renderedStarredRepositories: []
    }
  };

  componentDidUpdate = (prevProps) => {
    if(this.props.token !== prevProps.token) {
      if(this.props.token == null) {
        this.props.logout()
      }
    }
  }

  onLogout = () => {
    this.props.onLogout();
  }

  inputChangedHandler = (event, inputId, formKey) => {
    const currentState = this.state[formKey];
    const updatedForm = inputChangedHandler(event.target.value, inputId, currentState);
    this.setState({
     [formKey]: updatedForm
    });
  }

  submitInputHandler = (key) => {
    if(key === 'searchTerm') {
      this.searchGithubUsers();
    } else {
      this.filterLangType();
    }
  }

  searchGithubUsers = () => {
    const { searchForm } = this.state;
    const { token } = this.props;
    this.setState({ 
      isLoading: true, hasSearched: true 
    }, () => {
      githubClient.searchUsers(token, searchForm.searchTerm.value)
        .then(res => {
          const result = res.data;
          if(result.data != null) {
            const userData = result.data.search.edges[0].node;
            const starredRepositories = userData.starredRepositories.edges.map(repository => {
              const info = repository.node;
              const languages = info.languages.edges.map(lang => {
                return lang.node.name
              })
              return {
                description: info.description,
                id: info.id,
                name: info.nameWithOwner,
                languages: languages,
                primaryLanguage: info.primaryLanguage,
                stargazers: info.stargazers.totalCount,
                viewerHasStarred: info.viewerHasStarred
              }
            });

            const userObject = {
              bio: userData.bio,
              company: userData.company,
              email: userData.email,
              id: userData.id,
              location: userData.location,
              login: userData.login,
              name: userData.name,
              photo: userData.avatarUrl,
              url: userData.url,
              starredRepositories: starredRepositories,
              renderedStarredRepositories: starredRepositories
            }
            this.setState({ isLoading: false, hasFoundUser: true, userInformation: userObject })
          } else {
            this.setState({ isLoading: false, hasFoundUser: false })
          }        
        })
    })
  }

  handleStarRepository = ({ id, viewerHasStarred } = {}) => {
    const { popUp, userInformation } = this.state;
    const { token } = this.props;
    const action = viewerHasStarred ? 'removeStar' : 'addStar';

    githubClient.handleStar(token, id, action)
      .then(res => {
        const result = res.data.data
        if(result.removeStar != null || result.addStar) {
          const currentUserInfo = {...userInformation};
          const updatedRepoInformation = currentUserInfo.starredRepositories.map(repository => {
            if(repository.id === id) {
              repository.viewerHasStarred = !viewerHasStarred;
            }
            return repository;
          });
          const updatedUserInfo = updateObject(currentUserInfo, {
            starredRepositories: updatedRepoInformation
          });
          const popUpMessage = viewerHasStarred ? 'Repo unstarred with success!' : 'Repo starred with success!';
          const updatedPopMessage = updateObject(popUp, {
            messageText: popUpMessage
          })

          this.setState({ 
            popUp: updatedPopMessage, userInformation: updatedUserInfo 
          }, () => this.openPopHandler(popUp.key))
        }
      })
  }

  filterLangType = () => {
    const { userInformation, searchForm } = this.state;
    const currentSearchForm = {...searchForm};
    const currentUserInfo = {...userInformation};
    const filterTerm = currentSearchForm.langFilter.value;
    const starredRepositories = currentUserInfo.starredRepositories;

    const filteredRepos = starredRepositories.filter(repo => {
      return repo.languages.some(lang => lang.toLowerCase() === filterTerm.toLowerCase());
    })

    const updatedUserInfo = updateObject(currentUserInfo, {
      renderedStarredRepositories: filteredRepos
    })

    this.setState({ userInformation: updatedUserInfo })
  }

  openPopHandler = (key) => {
    const updatedPopUp = updateObject(this.state[key], {
      isActive: true
    });
    this.setState({[key]: updatedPopUp});
  }

  closePopHandler = (key) => {
    const updatedPopUp = updateObject(this.state[key], {
      isActive: false
    });
    this.setState({[key]: updatedPopUp});
  }

  render() {
    const { isLoading, hasSearched, hasFoundUser, popUp, searchForm, userInformation } = this.state;
    const { userName, userPhoto } = this.props;
    
    return (
      <div className='search'>
        {
          hasSearched
          ? (
              <div className='search__container'>
                <SearchHeader
                  inputChangedHandler={this.inputChangedHandler} 
                  inputForm={searchForm} 
                  inputField={searchForm.searchTerm.id}
                  onLogout={this.onLogout}
                  submitInputHandler={this.submitInputHandler}
                  userName={userName}
                  userPhoto={userPhoto}
                />
                {
                  isLoading
                  ? <Loading/>
                  : hasFoundUser
                    ? (
                        <div className='search__results'>
                          <Profile
                            userInformation={userInformation}
                          />
                          <CardList
                            inputChangedHandler={this.inputChangedHandler}
                            inputConfig={searchForm.langFilter.inputConfig}
                            inputForm={searchForm}
                            inputField={searchForm.langFilter.id}
                            handleStarRepository={this.handleStarRepository}
                            starredRepositories={userInformation.renderedStarredRepositories}
                            submitInputHandler={this.submitInputHandler}
                            value={searchForm.langFilter.value}
                          />
                          <PopUp
                            buttonText={popUp.buttonText}
                            dismissPopUp={this.closePopHandler}
                            isActive={popUp.isActive}
                            messageText={popUp.messageText}
                            onAction={this.closePopHandler}
                            popUpKey={popUp.key}
                          />
                        </div>
                      )
                    : (
                        <NoResultsFound
                          errorMessage='User not found'
                        />
                      )
                }
              </div>
            )
          : (
              <CenteredInput
                inputChangedHandler={this.inputChangedHandler} 
                inputForm={searchForm} 
                inputField={searchForm.searchTerm.id}
                submitInputHandler={this.submitInputHandler}
              />
            )
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.auth.isLoggedIn,
    token: state.auth.token,
    userName: state.auth.userName,
    userPhoto: state.auth.userPhoto
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogout: () => dispatch(logout()),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Login);
