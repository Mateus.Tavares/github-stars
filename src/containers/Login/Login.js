import React, { Component } from 'react';
import { connect } from 'react-redux';

import CenteredInput from '@molecules/CenteredInput';
import { inputChangedHandler } from '@utils';
import { githubClient } from '@client';
import { authSuccess } from '@actions';

import './Login.scss';

class Login extends Component {
  state = {
    loginError: false,
    loginForm: {
      key: 'loginForm',
      personalToken: {
        id: 'personalToken',
        inputConfig: {
          name: 'text',
          type: 'text',
          placeholder: 'Use your Github Personal token to login...'
        },
        value: ''
      },
    },
  };

  inputChangedHandler = (event, inputId, formKey) => {
    const currentState = this.state[formKey];
    const updatedForm = inputChangedHandler(event.target.value, inputId, currentState);
    this.setState({
     [formKey]: updatedForm
    });
  }

  submitInputHandler = () => {
    const { loginForm } = this.state;
    githubClient.loginToGitHub(loginForm.personalToken.value)
      .then(res => {
        const user = res.data.data.viewer;
        this.setState({ 
          loginError: false 
        }, () => this.props.onLogin(loginForm.personalToken.value, user.name, user.avatarUrl))
      })
      .catch(() => {
        this.setState({ loginError: true })
      })
  }

  render() {
    const { loginError, loginForm } = this.state;
    return (
      <div className='login'>
        <CenteredInput
          inputChangedHandler={this.inputChangedHandler} 
          inputForm={loginForm} 
          inputField={loginForm.personalToken.id}
          submitInputHandler={this.submitInputHandler}
        />
        {
          loginError
          ? (
              <h2 className='login__error'>Error logging in, please check if your personal token is correct and try again.</h2>
            )
          : null
        }
      </div>
    )
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: (token, name, photo) => dispatch(authSuccess(token, name, photo)),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Login);
