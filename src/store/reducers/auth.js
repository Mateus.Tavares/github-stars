import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '@utils';

const initialState = {
  isLoggedIn: null,
  token: null,
  userName: '',
  userPhoto: ''
};

const authLogout = (state, action) => {
  return updateObject(state, {
    isLoggedIn: false,
    token: null,
    userName: '',
    userPhoto: '',
  });
}

const authSuccess = (state, action) => {
  return updateObject(state, {
    isLoggedIn: true,
    token: action.token,
    userName: action.userName,
    userPhoto: action.userPhoto,
  });
};

const reducer = (state = initialState, action) => {
  switch(action.type){
    case actionTypes.AUTH_LOGOUT: return authLogout(state, action);
    case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
    default: return state;
  }
};

export default reducer;
