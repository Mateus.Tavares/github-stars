import * as actionTypes from './actionTypes';

export const authSuccess = (token, userName, userPhoto) => {
  localStorage.setItem('token', token);
  localStorage.setItem('userName', userName);
  localStorage.setItem('userPhoto', userPhoto);
  return {
    type: actionTypes.AUTH_SUCCESS,
    token: token,
    userName: userName,
    userPhoto: userPhoto
  };
};

export const logout = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('userName');
  localStorage.removeItem('userPhoto');
  return {
    type: actionTypes.AUTH_LOGOUT
  }
}

export const authAutoLogin = () => {
  const token = localStorage.getItem('token');

  return dispatch => {
    if(!token){
      dispatch(logout());
    } else {
      const userName = localStorage.getItem('userName');
      const userPhoto = localStorage.getItem('userPhoto');
      dispatch(authSuccess(token, userName, userPhoto));
    }
  };
};
